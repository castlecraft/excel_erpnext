import frappe


def delete_tokens():
    access_tokens = frappe.get_all("OAuth Bearer Token")
    for token in access_tokens:
        frappe.delete_doc("OAuth Bearer Token", token.name)
    frappe.db.commit()
